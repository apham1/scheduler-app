import { Time } from './time';
import { Days } from './days';

export class Course {
  name: string;
  days: Days;
  start_time: Time;
  end_time: Time;

  /**
   * Course class represents the information about a course.
   * All fields are mandatory.
   * @param name Name of the course
   * @param days Days of the course
   * @param start_time Starting time of the course
   * @param end_time Ending time of the course
   */
  constructor(name: string, days: Days, start_time: Time, end_time: Time) {
    this.name = name;
    this.days = days;
    this.start_time = start_time;
    this.end_time = end_time;
  }

  /**
   * Gets the length of the class in minutes.
   * @returns {number} length of class
   */
  getLength(): number {
    return (this.end_time.getHour() - this.start_time.getHour()) * 60 + (this.end_time.getMinutes() - this.start_time.getMinutes());
  }

  /**
   * Gets the amount of classes per week.
   * @returns {number} Amount of classes per week
   */
  getClasses(): number {
    return this.days.getDays();
  }

  /**
   * Creates a clone of type Course.
   * @returns {Course} Course created
   */
  clone(): Course {
    let name = this.name;
    let days = new Days(this.days.monday, this.days.tuesday, this.days.wednesday, this.days.thursday, this.days.friday);
    let start_time = new Time(this.start_time.hour, this.start_time.minutes, this.start_time.PM);
    let end_time = new Time(this.end_time.hour, this.end_time.minutes, this.end_time.PM);
    return new Course(name, days, start_time, end_time);
  }
}

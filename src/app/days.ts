export class Days {
  monday: boolean;
  tuesday: boolean;
  wednesday: boolean;
  thursday: boolean;
  friday: boolean;
  saturday: boolean;
  sunday: boolean;

  /**
   * Days class represents days of the course.
   * @param {boolean} monday
   * @param {boolean} tuesday
   * @param {boolean} wednesday
   * @param {boolean} thursday
   * @param {boolean} friday
   */
  constructor(monday: boolean, tuesday: boolean, wednesday: boolean, thursday: boolean, friday: boolean) {
    this.monday = monday;
    this.tuesday = tuesday;
    this.wednesday = wednesday;
    this.thursday = thursday;
    this.friday = friday;
    this.saturday = false;
    this.sunday = false;
  }

  /**
   * Returns days in string format.
   * @returns {string} days
   */
  toString(): string {
    let days = '';

    if (this.monday) {
      days += 'M ';
    }
    if (this.tuesday) {
      days += 'T ';
    }
    if (this.wednesday) {
      days += 'W ';
    }
    if (this.thursday) {
      days += 'Th ';
    }
    if (this.friday) {
      days += 'F ';
    }
    if (this.saturday) {
      days += 'S ';
    }
    if (this.sunday) {
      days += 'Su ';
    }

    return days.trim();
  }

  /**
   * Gets the amount of days that are true.
   * @returns {number}
   */
  getDays(): number {
    let count = 0;
    if (this.monday) {
      count++;
    }
    if (this.tuesday) {
      count++;
    }
    if (this.wednesday) {
      count++;
    }
    if (this.thursday) {
      count++;
    }
    if (this.friday) {
      count++;
    }
    if (this.saturday) {
      count++;
    }
    if (this.sunday) {
      count++;
    }

    return count;
  }
}

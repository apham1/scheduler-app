import { Component, Input } from '@angular/core';
import { Course } from '../../course';

@Component({
  selector: 'app-new-schedule',
  templateUrl: './new-schedule.component.html',
  styleUrls: ['./new-schedule.component.css']
})
export class NewScheduleComponent {
  @Input() courses: Array<Course>;

  constructor() {}

  onClick() {
    this.courses.splice(0, this.courses.length);
  }
}

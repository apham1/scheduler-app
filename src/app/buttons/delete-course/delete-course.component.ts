import { Component, Input } from '@angular/core';
import { Course } from '../../course';

@Component({
  selector: 'app-delete-course',
  templateUrl: './delete-course.component.html',
  styleUrls: ['./delete-course.component.css']
})
export class DeleteCourseComponent {
  @Input() courses: Array<Course>;

  constructor() {}

  onDelete(course: Course) {
    const index = this.courses.indexOf(course, 0);
    if (index > -1) {
      this.courses.splice(index, 1);
    }
  }
}

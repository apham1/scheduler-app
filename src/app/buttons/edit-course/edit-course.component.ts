import { Component, Input } from '@angular/core';
import { Course } from '../../course';
import { Days } from '../../days';
import { Time } from '../../time';

@Component({
  selector: 'app-edit-course',
  templateUrl: './edit-course.component.html',
  styleUrls: ['./edit-course.component.css']
})
export class EditCourseComponent {
  @Input() courses: Array<Course>;
  name: string;
  days: Days;
  start_time: Time;
  end_time: Time;

  index: number;

  constructor() {}

  onSelect(course: Course) {
    this.index = this.courses.indexOf(course, 0);
    if (this.index > -1) {
      let course = this.courses[this.index].clone();
      this.name = course.name;
      this.days = course.days;
      this.start_time = course.start_time;
      this.end_time = course.end_time;
    }
  }

  onEdit() {
    if (!this.start_time.PM && this.start_time.hour < 8) {
      console.log('less than 8am');
    } else if (this.end_time.PM && this.end_time.hour > 6) {
      console.log('greater than 6pm');
    } else if ((((!this.start_time.PM && !this.end_time.PM) || (this.start_time.PM && this.end_time.PM))
        && (this.start_time.hour >= this.end_time.hour && this.start_time.minutes >= this.end_time.minutes))
      || (this.start_time.PM && !this.end_time.PM)) {
      console.log('start time greater than or equal to end time');
    } else {
      this.courses[this.index].name = this.name;
      this.courses[this.index].days = this.days;
      this.courses[this.index].start_time = this.start_time;
      this.courses[this.index].end_time = this.end_time;
    }
  }
}

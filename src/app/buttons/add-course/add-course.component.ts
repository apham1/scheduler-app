import { Component, Input } from '@angular/core';
import { Course } from '../../course';
import { Days } from '../../days';
import { Time } from '../../time';

@Component({
  selector: 'app-add-course',
  templateUrl: './add-course.component.html',
  styleUrls: ['./add-course.component.css']
})
export class AddCourseComponent {
  @Input() courses: Array<Course>;
  name: string;
  days: Days = new Days(false, false, false, false, false);
  start_time: Time = new Time(8, 0, false);
  end_time: Time = new Time(9, 0, false);

  onSubmit() {
     if (!this.start_time.PM && this.start_time.hour < 8) {
       console.log('less than 8am');
     } else if (this.end_time.PM && this.end_time.hour > 6 && this.end_time.hour !== 12) {
       console.log('greater than 6pm');
     } else if ((((!this.start_time.PM && !this.end_time.PM) || (this.start_time.PM && this.end_time.PM))
              && (this.start_time.hour >= this.end_time.hour && this.start_time.minutes >= this.end_time.minutes))
              || (this.start_time.PM && !this.end_time.PM)) {
       console.log('start time greater than or equal to end time');
     } else {
       this.courses.push(new Course(this.name, this.days, this.start_time, this.end_time));
       this.name = '';
       this.days = new Days(false, false, false, false, false);
       this.start_time = new Time(8, 0, false);
       this.end_time = new Time(9, 0, false);
     }
  }

  constructor() {}
}

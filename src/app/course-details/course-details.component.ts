import { Component, OnInit, Input } from '@angular/core';
import { Course } from '../course';
import { COLORS } from '../colors';

@Component({
  selector: 'app-course-details',
  templateUrl: './course-details.component.html',
  styleUrls: ['./course-details.component.css']
})
export class CourseDetailsComponent implements OnInit {
  @Input() course: Course;
  @Input() course_list;
  colors = COLORS;
  color: string;

  constructor() {}

  ngOnInit() {
    this.color = this.generateColor();
  }

  /**
   * Gets the height of the box based off the length of the course.
   * @returns (number) height of box
   */
  getBoxHeight(): number {
    return this.course.getLength() * (4 / 3);
  }

  /**
   * Gets y position of the box based on the start time of the course.
   * @returns (number) y position of box
   */
  getYPos(): number {
    return 54 + ((this.course.start_time.getHour() - 8) * 60
      + (this.course.start_time.getMinutes())) * (4 / 3);
  }

  generateColor(): string {
    this.course_list.currentColorNum++;
    return this.colors[this.course_list.currentColorNum - 1];
  }
}

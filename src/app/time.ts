export class Time {
  hour: number;
  minutes: number;
  PM: boolean;

  /**
   * Time class represents time of a 12-hour clock.
   * @param {number} hour
   * @param {number} minutes
   * @param {boolean} PM
   */
  constructor(hour: number, minutes: number, PM: boolean) {
    this.hour = hour;
    this.minutes = minutes;
    this.PM = PM;
  }

  /**
   * Converts time to string format.
   * @returns {string}
   */
  toString(): string {
    let output_string = '';

    if ( this.minutes < 10 ) {
      output_string += this.hour.toString() + ':0' + this.minutes.toString();
    } else {
      output_string += this.hour.toString() + ':' + this.minutes.toString();
    }

    if ( this.PM ) {
      output_string += 'PM';
    } else {
      output_string += 'AM';
    }

    return output_string;
  }

  /**
   * Gets the hour of the time.
   * @returns {number} hour
   */
  getHour(): number {
    if (this.PM && this.hour !== 12) {
      return this.hour + 12;
    } else {
      return this.hour;
    }
  }

  /**
   * Gets the minutes of the time.
   * @returns {number} minutes
   */
  getMinutes(): number {
    return this.minutes;
  }
}

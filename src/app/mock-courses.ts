import { Course } from './course';
import { Time } from './time';
import { Days } from './days';

export const COURSES: Course[] = [
  new Course('CS373', new Days(false, true, false, true, false), new Time(8, 30, false), new Time(9, 45, false)),
  new Course('CS343', new Days(false, true, false, true, false), new Time(10, 0, false), new Time(11, 15, false)),
  new Course('CS248', new Days(true, false, true, false, false), new Time(8, 0, false), new Time(9, 15, false)),
  new Course('MA240', new Days(true, false, true, false, false), new Time(11, 30, false), new Time(1, 10, true)),
  new Course('MA310', new Days(true, true, true, true, false), new Time(1, 30, true), new Time(3, 10, true)),
  new Course('test', new Days(false, false, false, false, true), new Time( 8, 0, false), new Time(6, 0, true)),
  new Course('test2', new Days(false, false, false, true, false), new Time( 4, 0, true), new Time(5, 0, true))
];


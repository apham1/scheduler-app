import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { CourseListComponent } from './course-list/course-list.component';
import { CourseDetailsComponent } from './course-details/course-details.component';
import { AddCourseComponent } from './buttons/add-course/add-course.component';
import { DeleteCourseComponent } from './buttons/delete-course/delete-course.component';
import { EditCourseComponent } from './buttons/edit-course/edit-course.component';
import { NewScheduleComponent } from './buttons/new-schedule/new-schedule.component';


@NgModule({
  declarations: [
    AppComponent,
    CourseListComponent,
    CourseDetailsComponent,
    AddCourseComponent,
    DeleteCourseComponent,
    EditCourseComponent,
    NewScheduleComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
